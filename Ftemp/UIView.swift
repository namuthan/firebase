//
//  UIView.swift
//  X4DWorkPackages
//
//  Created by Nabil Muthanna on 2016-12-05.
//  Copyright © 2016 Nabil Muthanna. All rights reserved.
//


import UIKit



extension UIView {
    
    func setAsCardView() {
        layer.cornerRadius = 3.0
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.withAlphaComponent(0.2).cgColor
        layer.shadowOffset = CGSize(width: 0, height: 0)
        layer.shadowOpacity = 0.8
    }
    
}

extension UIView {
    
    /**
     Rounds the given set of corners to the specified radius
     
     - parameter corners: Corners to round
     - parameter radius:  Radius to round to
     */
    func round(corners: UIRectCorner, radius: CGFloat) {
        _round(corners: corners, radius: radius)
    }
    
    /**
     Rounds the given set of corners to the specified radius with a border
     
     - parameter corners:     Corners to round
     - parameter radius:      Radius to round to
     - parameter borderColor: The border color
     - parameter borderWidth: The border width
     */
    func round(corners: UIRectCorner, radius: CGFloat, borderColor: UIColor, borderWidth: CGFloat) {
        let mask = _round(corners: corners, radius: radius)
        addBorder(mask: mask, borderColor: borderColor, borderWidth: borderWidth)
    }
    
    /**
     Fully rounds an autolayout view (e.g. one with no known frame) with the given diameter and border
     
     - parameter diameter:    The view's diameter
     - parameter borderColor: The border color
     - parameter borderWidth: The border width
     */
    func fullyRound(diameter: CGFloat, borderColor: UIColor, borderWidth: CGFloat) {
        layer.masksToBounds = true
        layer.cornerRadius = diameter / 2
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor.cgColor;
    }
    
}

private extension UIView {
    
    @discardableResult func _round(corners: UIRectCorner, radius: CGFloat) -> CAShapeLayer {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
        return mask
    }
    
    func addBorder(mask: CAShapeLayer, borderColor: UIColor, borderWidth: CGFloat) {
        let borderLayer = CAShapeLayer()
        borderLayer.path = mask.path
        borderLayer.fillColor = UIColor.clear.cgColor
        borderLayer.strokeColor = borderColor.cgColor
        borderLayer.lineWidth = borderWidth
        borderLayer.frame = bounds
        layer.addSublayer(borderLayer)
    }
    
}


// MARK : - Constrraints


extension UIView {
    
    enum LayoutEdges {
        case left, top, right, bottom, horizontalCentering, verticalCentering
    }
    
    enum Axise {
        case horizontal, vertical
    }
    
    func addSubviews(_ subviews: [UIView]) {
        for view in subviews {
            addSubview(view)
        }
    }
    
    func constrainEdges(toMarginOf anotherView: UIView, withMargin margin: CGFloat = 0) {
        
        NSLayoutConstraint.activate([
            
            leftAnchor.constraint(equalTo: anotherView.leftAnchor, constant: margin),
            topAnchor.constraint(equalTo: anotherView.topAnchor, constant: margin),
            rightAnchor.constraint(equalTo: anotherView.rightAnchor, constant: -margin),
            bottomAnchor.constraint(equalTo: anotherView.bottomAnchor, constant: -margin),
            
            ])
    }
    
    func center(inView anotherView: UIView) {
        NSLayoutConstraint.activate([
            
            centerXAnchor.constraint(equalTo: anotherView.centerXAnchor),
            centerYAnchor.constraint(equalTo: anotherView.centerYAnchor),
            
            ])
    }
    
    func addEdgesConstraints(left: NSLayoutAnchor<NSLayoutXAxisAnchor>, top: NSLayoutAnchor<NSLayoutYAxisAnchor>,
                             right: NSLayoutAnchor<NSLayoutXAxisAnchor>, bottom: NSLayoutAnchor<NSLayoutYAxisAnchor>) {
        NSLayoutConstraint.activate([
            
            leftAnchor.constraint(equalTo: left ),
            topAnchor.constraint(equalTo: top),
            rightAnchor.constraint(equalTo: right),
            bottomAnchor.constraint(equalTo: bottom),
        ])
    }
    
    func center(axises: [Axise], inView view: UIView) {
        for axis in axises {
            center(axis: axis, inView: view)
        }
    }
    
    func center(axis: Axise, inView view: UIView) {
        switch axis {
        case .horizontal:
            centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        case .vertical:
            centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        }
    }
    
    func layout(edges: [LayoutEdges], toEdges otherEdges: [LayoutEdges], ofViews views: [UIView], withMargins margins: [Float]) {
        guard (edges.count == otherEdges.count) && (otherEdges.count == views.count) && (views.count == margins.count) else { return }
        for (index, edge) in edges.enumerated() {
            layout(edge: edge, toEdge: otherEdges[index], ofView: views[index], withMargin: margins[index])
        }
    }
    
    func layout(edge: LayoutEdges, toEdge otherEdge: LayoutEdges, ofView view: UIView, withMargin margin: Float) {
    
        switch (edge, otherEdge) {
        case (.top, .top):
            topAnchor.constraint(equalTo: view.topAnchor, constant: CGFloat(margin)).isActive = true
        case (.top, .bottom):
            topAnchor.constraint(equalTo: view.bottomAnchor, constant: CGFloat(margin)).isActive = true
        case (.bottom, .top):
            bottomAnchor.constraint(equalTo: view.topAnchor, constant: -CGFloat(margin)).isActive = true
        case (.bottom, .bottom):
            bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -CGFloat(margin)).isActive = true
        case (.left, .left):
            leftAnchor.constraint(equalTo: view.leftAnchor, constant: CGFloat(margin)).isActive = true
        case (.left, .right):
            leftAnchor.constraint(equalTo: view.rightAnchor, constant: CGFloat(margin)).isActive = true
        case (.right, .left):
            rightAnchor.constraint(equalTo: view.leftAnchor, constant: -CGFloat(margin)).isActive = true
        case (.right, .right):
            rightAnchor.constraint(equalTo: view.rightAnchor, constant: -CGFloat(margin)).isActive = true
        case (.horizontalCentering, .horizontalCentering):
            center(axis: .horizontal, inView: view)
        case (.verticalCentering, .verticalCentering):
            center(axis: .vertical, inView: view)
        default:
            break
        }
    }
   
    func layoutTo(edges: [LayoutEdges], ofView anotherView: UIView, withMargin margin: Float) {
        for edge in edges {
            layoutTo(edge: edge, ofView: anotherView, withMargin: margin)
        }
    }
    
    func layoutTo(edge: LayoutEdges, ofView anotherView: UIView, withMargin margin: Float) {
        switch edge {
        case .left:
                leftAnchor.constraint(equalTo: anotherView.leftAnchor, constant: CGFloat(margin)).isActive = true
        case .top:
                topAnchor.constraint(equalTo: anotherView.topAnchor, constant: CGFloat(margin)).isActive = true
        case .right:
            rightAnchor.constraint(equalTo: anotherView.rightAnchor, constant: -CGFloat(margin)).isActive = true
        case .bottom:
            bottomAnchor.constraint(equalTo: anotherView.bottomAnchor, constant: -CGFloat(margin)).isActive = true
        case .horizontalCentering:
            center(axis: .horizontal, inView: anotherView)
        case .verticalCentering:
            center(axis: .vertical, inView: anotherView)
        }
    }
}


// MARK: - Animation

extension UIView {
    
    func rotate(degrees: Float, duration: TimeInterval = 1.0, completionDelegate: CAAnimationDelegate? = nil) {
        
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(degrees)
        rotateAnimation.duration = duration
        
        if let delegate: CAAnimationDelegate = completionDelegate {
            rotateAnimation.delegate = delegate
        }
        self.layer.add(rotateAnimation, forKey: nil)
    }
    
    func slideInFromLeft(duration: TimeInterval = 1.0, completionDelegate: CAAnimationDelegate? = nil) {
        // Create a CATransition animation
        let slideInFromLeftTransition = CATransition()
        
        // Set its callback delegate to the completionDelegate that was provided (if any)
        if let delegate: CAAnimationDelegate = completionDelegate {
            slideInFromLeftTransition.delegate = delegate
        }
        
        // Customize the animation's properties
        slideInFromLeftTransition.type = kCATransitionPush
        slideInFromLeftTransition.subtype = kCATransitionFromLeft
        slideInFromLeftTransition.duration = duration
        slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        slideInFromLeftTransition.fillMode = kCAFillModeRemoved
        
        // Add the animation to the View's layer
        self.layer.add(slideInFromLeftTransition, forKey: "slideInFromLeftTransition")
    }
}
