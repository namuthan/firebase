//
//  UITableView.swift
//  X4D
//
//  Created by Nabil Muthanna on 2017-01-09.
//  Copyright © 2017 Nabil Muthanna. All rights reserved.
//

import UIKit

extension UITableView {
    
    func register<T: UITableViewCell>(_: T.Type) where T: ReusableView {
        register(T.self, forCellReuseIdentifier: T.reuseIdentifier)
    }
    
    func dequeueReusableCell<T: UITableViewCell>(forIndexPath indexPath: IndexPath) -> T where T: ReusableView {
        guard let cell = dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
            fatalError("Could not dequeue cell with identifier: \(T.reuseIdentifier)")
        }
        return cell
    }
    
    func cell<T: UITableViewCell>(forIndexPath indexPath: IndexPath) -> T? {
        guard let cell = cellForRow(at: indexPath) as? T else {
            return nil
        }
        return cell
    }
}


extension UITableViewCell: ReusableView {}
