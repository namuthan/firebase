//
//  Guest.swift
//  Ftemp
//
//  Created by Nabil Muthanna on 2017-02-16.
//  Copyright © 2017 Nabil Muthanna. All rights reserved.
//

import FirebaseDatabase


enum GusetStatus: String {
    case going, notGoing, maybe, notKnown
}


struct Guest {
    
    let key: String // user uid
    let email: String
    let userName: String
    var stauts: GusetStatus
    let profileImageUrl: URL?
    
}

extension Guest: JSONDecodable {
    enum Keys: String { case email, userName, profileImageUrl, stauts }
    
    init?(snapShot: FIRDataSnapshot) {
        guard let dictionary = snapShot.value => JSONDictionary.self,
            let email = dictionary.string(key: Keys.email.rawValue),
            let userName = dictionary.string(key: Keys.userName.rawValue),
            let status = dictionary.string(key: Keys.stauts.rawValue) else {
                return nil
        }
        self.key = snapShot.key
        self.email = email
        self.userName = userName
        self.profileImageUrl = dictionary.url(key: Keys.profileImageUrl.rawValue)
        self.stauts = GusetStatus(rawValue: status)!
    }
    
    func convertToJson() -> JSONDictionary {
        var data = [
            Keys.email.rawValue: email,
            Keys.userName.rawValue: userName,
        ]
        profileImageUrl >>>= { data[Keys.profileImageUrl.rawValue] = $0.absoluteString }
        return data
    }
}
