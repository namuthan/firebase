//
//  UIViewController.swift
//  X4DWorkPackages
//
//  Created by Nabil Muthanna on 2016-12-08.
//  Copyright © 2016 Nabil Muthanna. All rights reserved.
//

import UIKit


extension UIViewController {
    
    func add(childViewController controller: UIViewController) {
        addChildViewController(controller)
        view.addSubview(controller.view)
        controller.view.translatesAutoresizingMaskIntoConstraints = false
        controller.view.layoutTo(edges: [.left, .top, .right, .bottom], ofView: view, withMargin: 0)
        controller.didMove(toParentViewController: self)
    }
    
    func remove(childViewController controller: UIViewController) {
        controller.willMove(toParentViewController: nil)
        controller.view.removeFromSuperview()
        controller.removeFromParentViewController()
    }
    
   
    func presentPopover(vc:UIViewController, withSourceView sourceView: UIView, andArrowDirection arrowDirection: UIPopoverArrowDirection) {
        vc.modalPresentationStyle = .popover
        let popoverVC = vc.popoverPresentationController
        popoverVC?.backgroundColor = MainStyleSheet.Theme.main.color
        popoverVC?.permittedArrowDirections = arrowDirection
        
        popoverVC?.delegate = self
        
        popoverVC?.sourceView = sourceView
        popoverVC?.sourceRect = sourceView.bounds
        present(vc, animated: true, completion: nil)
    }
    
    func updateActiveViewController(activeVC: UIViewController, contentView: UIView) {
        // call before adding child view controller's view as subview
        addChildViewController(activeVC)
        
        activeVC.view.frame = contentView.bounds
        contentView.addSubview(activeVC.view)
        
        // call before adding child view controller's view as subview
        activeVC.didMove(toParentViewController: self)
    }
    
    
    func alertUser(title: String, error: NSError) {
        
        let message = "\(error.userInfo[NSLocalizedDescriptionKey]!)"
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
            alertController.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
//    func createErrorObject(_ withErrorMessage: String, andErrorCode: Int) -> NSError{
//        let userInfo = [
//            NSLocalizedDescriptionKey: NSLocalizedString(withErrorMessage, comment: "")
//        ]
//        
//        let error = NSError(domain: X4DNetworkingErrorDomain, code: andErrorCode, userInfo: userInfo)
//        
//        return error
//    }
    
    func removeInactiveViewController(inactiveViewController: UIViewController?) {
        if let inActiveVC = inactiveViewController {
            // call before removing child view controller's view from hierarchy
            inActiveVC.willMove(toParentViewController: nil)
            
            inActiveVC.view.removeFromSuperview()
            
            // call after removing child view controller's view from hierarchy
            inActiveVC.removeFromParentViewController()
        }
    }
    

}


// MARK: - UIPopoverPresentationControllerDelegate

extension UIViewController: UIPopoverPresentationControllerDelegate {
    
    @objc public func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    @objc public func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
}


