//
//  APIService.swift
//  Ftemp
//
//  Created by Nabil Muthanna on 2017-03-04.
//  Copyright © 2017 Nabil Muthanna. All rights reserved.
//

import FirebaseDatabase
import PromiseKit


extension FIRDataSnapshot {
    
    func items<T: JSONDecodable>() -> [T]? {
        return (self.children.allObjects => [FIRDataSnapshot].self)?.flatMap { T($0) }
    }
    
    func item<T: JSONDecodable>() -> T? {
        return (self) >>>= { T($0) }
    }
}


// MARK : - HtppMethod

enum HttpMethod<T> {
    case get
    case post(T)
    case put(T)
    case delete
}

extension HttpMethod {
    func map<B>(f: (T) -> B) -> HttpMethod<B> {
        switch self {
        case .get: return .get
        case .post(let body): return .post(f(body))
        case .put(let body): return .put(f(body))
        case .delete: return .delete
        }
    }
}

// MARK : - Resource

struct Resource<T> {
    
    var query: FIRDatabaseQuery
    let method: HttpMethod<T> = .get
    var eventType: FIRDataEventType
    let parse: ((FIRDataSnapshot) -> Result<T>)
}


// MARK: - APIResult

enum Result<T>{
    case success(T)
    case failure(Error)
}



// MARK: - APIService

class WebService {
    
    static let shared = WebService()

    func perform<T>(_ resource: Resource<T>, completion: @escaping (Result<T>) -> ()) {
        
        resource.query.ref.observe(resource.eventType, with: { (snapshot) in
            self.dispatchToMainThread(resource.parse(snapshot), completion: completion)
        })
        
        
    }

    func perform<T>(_ resource: Resource<T>) -> Promise<T> {
        
        return Promise { fulfill, reject in
            perform(resource) { result in
                switch result {
                case .success(let data):
                    fulfill(data)
                case .failure(let err):
                    reject(err)
                }
            }
        }
    }
    
    // MARK: - Helper Methods
    
 
    
    func dispatchToMainThread<T>(_ result: Result<T>, completion: @escaping (Result<T>) -> Void) {
        DispatchQueue.main.async(execute: { () -> Void in
            completion(result)
        })
    }
    
}



