//
//  UserEndPoints.swift
//  Ftemp
//
//  Created by Nabil Muthanna on 2017-03-26.
//  Copyright © 2017 Nabil Muthanna. All rights reserved.
//

import FirebaseDatabase


// MARK: Users

enum UserEndPoints: EndPoint {
    
    case load()
    case loadWithKey(String)
    case loadFollowers(String)
    case loadFollowings(String)
    case loadSubScriptions(String)
    
    case save()
    
    var ref: FIRDatabaseReference {
        get {
            switch self {
            case .load():
                return root.child(Briotie.RootKeys.users.rawValue)
            case .loadWithKey(let key):
                return root.child(Briotie.RootKeys.users.rawValue).child(key)
            case .loadFollowers(let key):
                return root.child(Briotie.RootKeys.followers.rawValue).child(key)
            case .loadFollowings(let key):
                return root.child(Briotie.RootKeys.followings.rawValue).child(key)
            case .loadSubScriptions(let key):
                return root.child(Briotie.RootKeys.subscriptions.rawValue).child(key)
                
                
            case .save():
                return root.child(Briotie.RootKeys.users.rawValue)
            }
        }
    }
}
        
