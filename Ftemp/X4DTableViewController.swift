//
//  Temp.swift
//  X4D
//
//  Created by Nabil Muthanna on 2017-01-06.
//  Copyright © 2017 Nabil Muthanna. All rights reserved.
//

import UIKit


class DefaultCell: UITableViewCell {}


struct CellDescriptor {
    let cellClass: UITableViewCell.Type
    let reuseIdentifier: String
    let configure: (UITableViewCell) -> ()
    
    init<Cell: UITableViewCell>(configure: @escaping (Cell) -> ()) where Cell: ReusableView {
        self.cellClass = Cell.self
        self.reuseIdentifier = Cell.reuseIdentifier
        self.configure = { cell in
            configure(cell as! Cell)
        }
    }
    
    static var cellDefault: CellDescriptor {
        return CellDescriptor() { (cell: DefaultCell) in
        }
    }
}

enum TableScrolDirection {
    case up, down
}


struct TableSection<T: Equatable> {
    var headerView: UIView?
    var height: Float
    var items: [T]
    var displayedItems: [T]
    
    
    init(items: [T], displayedItems: [T]? = nil, headerView: UIView? = nil, height: Float = 0) {
        self.items = items
        self.displayedItems = displayedItems ?? items
        self.headerView = headerView
        self.height = height
    }
}


class X4DTableViewController<Item: Equatable>: UITableViewController, UITableViewDataSourcePrefetching {
    
    var sections: [TableSection<Item>] = []
    var cellDescriptor: (Item, IndexPath)-> CellDescriptor
    var didSelect: (Item, IndexPath) -> () =  { _ in }
    
    var configureHeight: (Item, IndexPath) -> Float? = { _, _ in return nil }
    var scroll: (TableScrolDirection) -> () = { _ in }
    var reuseIdentifiers: Set<String> =  []
    
    var fetchData: ((_ forIndexPath: [IndexPath], _ withItems: [Item]) -> ())?
    
    fileprivate var lastContentOffset: CGPoint?
    
    init(sections: [TableSection<Item>], cellDescriptor: @escaping (Item, IndexPath) -> CellDescriptor) {
        self.sections = sections
        self.cellDescriptor = cellDescriptor
        super.init(nibName: nil, bundle: nil)
        self.tableView.reloadData()
        tableView.estimatedRowHeight = 100.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        if #available(iOS 10.0, *) {
            self.tableView.prefetchDataSource = self
        } else {
            // Fallback on earlier versions
        }
    }
    
    init(sections: [TableSection<Item>]) {
        self.sections = sections
        self.cellDescriptor =  { _, _ in return CellDescriptor.cellDefault }
        super.init(nibName: nil, bundle: nil)
        self.tableView.reloadData()
        tableView.estimatedRowHeight = 100.0
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.backgroundColor = MainStyleSheet.Theme.background.color
        tableView.separatorStyle = .none
        tableView.bounces = false
        
        // Allow buttons to be heighlighted when cliced on a cell
        tableView.delaysContentTouches = false
        for view in tableView.subviews {
            if let scroll = view as? UIScrollView {
                scroll.delaysContentTouches = false
            }
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].displayedItems.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item = sections[indexPath.section].displayedItems[indexPath.row]
        return CGFloat(configureHeight(item, indexPath) ?? Float(tableView.rowHeight))
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = sections[indexPath.section].displayedItems[indexPath.row]
        let descriptor = cellDescriptor(item, indexPath)
        if !reuseIdentifiers.contains(descriptor.reuseIdentifier) {
            tableView.register(descriptor.cellClass, forCellReuseIdentifier: descriptor.reuseIdentifier)
            reuseIdentifiers.insert(descriptor.reuseIdentifier)
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: descriptor.reuseIdentifier, for: indexPath)
        cell.selectionStyle = .none
        descriptor.configure(cell)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = sections[indexPath.section].displayedItems[indexPath.row]
        didSelect(item, indexPath)
    }
    
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        let items = indexPaths.map { self.sections[$0.section].items[$0.row] }
        self.fetchData?(indexPaths, items)
    }
    
    // MARK: - Insertion
    
    func insert(section: TableSection<Item>, atIndex index: Int?) {
        let sectionIndex = index ?? self.sections.count
        
        //update datasource
        self.sections.insert(section, at: sectionIndex)
        
        //update UI
        tableView.beginUpdates()
        tableView.insertSections([sectionIndex], with: .automatic)
        tableView.endUpdates()
    }
    
    func insert(sections: [TableSection<Item>], startingIndex: Int?) {
        let sectionIndex = startingIndex ?? self.sections.count
        for (index, item) in sections.enumerated() {
            insert(section: item, atIndex: (sectionIndex + index))
        }
    }

    func insert(items: [Item], forSection section: Int) {
        guard section < self.sections.count else { return }
        
        var previousIndex = self.sections[section].items.count
        
        // update the datasource
        self.sections[section].items += items
        self.sections[section].displayedItems += items
        
        //update the view
        tableView.beginUpdates()
        var indexPaths = [IndexPath]()
        _ = items.map({  _ in
            indexPaths.append(IndexPath(row: previousIndex, section: section))
            previousIndex += 1
        })
        tableView?.insertRows(at: indexPaths, with: .automatic)
        tableView.endUpdates()
    }
    
    // MARK : Update
    
    func update(section: Int, withItems items: [Item]) {
        guard section < self.sections.count else { return }
        self.sections[section].items = items
        self.sections[section].displayedItems = items
        self.tableView.reloadSections([section], with: .automatic)
    }
    
    func update(items: [Item], forSection section: Int) {
        guard section < self.sections.count else { return }
        _ = items.map { self.update(item: $0, forSection: section) }
    }

    fileprivate func update(item: Item, forSection section: Int) {
        guard section < self.sections.count else { return }
        if let index = self.sections[section].items.index(of: item) {
            self.sections[section].items[index] = item
            if let displayedItemsIndex = self.sections[section].displayedItems.index(of: item) {
                self.sections[section].displayedItems[displayedItemsIndex] = item
                self.reload(item: item, inSection: section)
            }
        }
    }
    
    // MARK: - reload
    
    func reload() {
        self.tableView.reloadData()
    }
    
    func reload(items: [Item], inSection section: Int) {
        let _ =  items.map({ item in reload(item: item, inSection: section)})
    }
    
    func reload(item: Item, inSection section: Int) {
        guard section < self.sections.count else { return }
        if let displayedItemsIndex = self.sections[section].displayedItems.index(of: item) {
            let indexPath = IndexPath(row: displayedItemsIndex, section: section)
            self.tableView.reloadRows(at: [indexPath], with: .automatic)
        }
    }
    
    func reload(at indexPaths: [IndexPath]) {
        self.tableView.reloadRows(at: indexPaths, with: .automatic)
    }
    
    func reload(section: Int) {
        self.tableView.reloadSections([section], with: .automatic)
    }
  
    // MARK: - Cell
    
    func cell<T: UITableViewCell>(forItem item: Item, inSection section: Int) -> T? {
        guard section < self.sections.count,
        let index = self.sections[section].displayedItems.index(of: item) else { return nil }
        return self.tableView.cellForRow(at: IndexPath(item: index, section: section)) as? T
    }
    
    func cell<T: UITableViewCell>(forIndexPath indexPath: IndexPath) -> T? {
        return (self.tableView.cellForRow(at: indexPath) as? T ?? nil)
    }
   
    // MARK: - Utilities
    
    func toggle(section: Int, exceptItem item: Item? = nil) {
        guard section < self.sections.count else { return }
        let count = item != nil ? 1 : 0
        self.sections[section].displayedItems.count == count ? showSection(section: section) : hide(section: section, exceptItem: item)
    }
    
    func hide(section: Int, exceptItem item: Item? = nil) {
        guard section < self.sections.count else { return }
        self.sections[section].displayedItems = item != nil ? [item!] : []
        self.reload(section: section)
    }
    
    func showSection(section: Int) {
        guard section < self.sections.count else { return }
        self.sections[section].displayedItems = self.sections[section].items
        self.reload(section: section)
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if let lastContentOffset = lastContentOffset {
            let currentPosition = scrollView.contentOffset
            if currentPosition.y > lastContentOffset.y {
                scroll(.down)
            } else {
                scroll(.up)
            }
            self.lastContentOffset = currentPosition;
        } else {
            lastContentOffset = scrollView.contentOffset
        }
    }
}
