//
//  ReusableView.swift
//  X4D
//
//  Created by Nabil Muthanna on 2017-02-06.
//  Copyright © 2017 Nabil Muthanna. All rights reserved.
//

import UIKit


protocol ReusableView: class {}

extension ReusableView where Self: UIView {
    
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}
