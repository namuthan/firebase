//
//  RaterMatrix.swift
//  Ftemp
//
//  Created by Nabil Muthanna on 2017-03-26.
//  Copyright © 2017 Nabil Muthanna. All rights reserved.
//

import Foundation

struct RaterMatrix {
    
    var upVotes: Int = 0
    var downVotes: Int = 0
    var reportCount: Int = 0
    var emberssionCount: Int = 0
    
}

extension RaterMatrix: JSONDecodable {
    
    enum Keys: String {
        case upVotes, downVotes, reportCount, emberssionCount
    }
    
    
    init?(_ dictionary: JSONDictionary) {
        
        self.upVotes = dictionary.int(key: Keys.upVotes.rawValue, or: 0)
        self.downVotes = dictionary.int(key: Keys.downVotes.rawValue, or: 0)
        self.reportCount = dictionary.int(key: Keys.reportCount.rawValue, or: 0)
        self.emberssionCount = dictionary.int(key: Keys.emberssionCount.rawValue, or: 0)
        
    }
    
    func convertToJson() -> JSONDictionary {
        return [
            Keys.upVotes.rawValue: upVotes,
            Keys.downVotes.rawValue: downVotes,
            Keys.reportCount.rawValue: reportCount,
            Keys.emberssionCount.rawValue: emberssionCount
        ]
    }
    
}
