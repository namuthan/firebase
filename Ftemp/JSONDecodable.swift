//
//  PR_JSONDecodable.swift
//  X4D
//
//  Created by Nabil Muthanna on 2017-02-06.
//  Copyright © 2017 Nabil Muthanna. All rights reserved.
//

import Foundation
import FirebaseDatabase

typealias JSONDictionary =  [String: Any]
typealias JSONArray = [JSONDictionary]

protocol JSONDecodable {
    
    init?(_ dictionary: JSONDictionary)
    init?(_ snapShot: FIRDataSnapshot)
    func convertToJson() -> JSONDictionary
}


extension JSONDecodable {
    
    init?(_ dictionary: JSONDictionary) { return nil }
    init?(_ snapShot: FIRDataSnapshot) { return nil }
    func convertToJson() -> JSONDictionary { return [:] }
    
}
