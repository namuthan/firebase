
//
//  LocationManager.swift
//  Ftemp
//
//  Created by Nabil Muthanna on 2017-03-11.
//  Copyright © 2017 Nabil Muthanna. All rights reserved.
//

import Foundation

class LocationManager {
    
    
    static let shared = LocationManager()
    
    // MARK: - Computed Propeties
    
    var newId: String {
        get {
            return LocationEndPoints.load().ref.childByAutoId().key
        }
    }
}
