//
//  BaseEndPoints.swift
//  Ftemp
//
//  Created by Nabil Muthanna on 2017-03-26.
//  Copyright © 2017 Nabil Muthanna. All rights reserved.
//

import FirebaseDatabase


struct DateRangeQuery {
    
    let startDate: Date
    let endDate: Date
}

struct PagedQuery {
    var lastItemKey: String? = nil
    var pageSize = 10
    
    init(lastItemKey: String? = nil, pageSize: Int = 10) {
        self.lastItemKey = lastItemKey
        self.pageSize = pageSize
    }
}



// MARK : - EndPoint

protocol EndPoint {
    var ref: FIRDatabaseReference { get }
    var root: FIRDatabaseReference { get }
}

extension EndPoint {
    var root: FIRDatabaseReference  {
        get {
            return FIRDatabase.database().reference()
        }
    }
}

enum Briotie {
    
    static var root: FIRDatabaseReference {
        get {
            return FIRDatabase.database().reference()
        }
    }
    
    enum RootKeys: String {
        
        case users
        case locations
        
        case events
        case eventComments
        case guests
        
        case discussions
        case discussionComments
        
        case commentReplies
     
        case followers
        case followings
        case subscriptions
        
        case interests
        case interestEvents
        
    }
}

