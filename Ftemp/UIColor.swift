//
//  UIColor.swift
//  X4D
//
//  Created by Nabil Muthanna on 2016-11-07.
//  Copyright © 2016 Nabil Muthanna. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    convenience init(colorWithHexValue value: Int, alpha:CGFloat = 1.0){
        self.init(
            red: CGFloat((value & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((value & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(value & 0x0000FF) / 255.0,
            alpha: alpha
        )
    }
    
    static func rgb(r: CGFloat, g: CGFloat, b: CGFloat) -> UIColor {
        return UIColor(red: r/255, green: g/255, blue: b/255, alpha: 1)
    }
    
    var red: CGFloat{
        return self.cgColor.components![0]
    }
    
    var green: CGFloat{
        return self.cgColor.components![1]
    }
    
    var blue: CGFloat{
        return self.cgColor.components![2]
    }
    
    var alpha: CGFloat{
        return self.cgColor.components![3]
    }
}
