//
//  X4DListController.swift
//  X4D
//
//  Created by Nabil Muthanna on 2017-01-04.
//  Copyright © 2017 Nabil Muthanna. All rights reserved.
//

import UIKit

class X4DListController<Item: Equatable, Cell: UITableViewCell>: X4DTableViewController<Item> where Cell: ReusableView {
    
    var configure: ((Item, Cell)->())?
    var didSelectWithCell: ((Item, IndexPath, Cell)  -> Void)?
    
    
    init(items: [Item], configure: @escaping((Item, Cell)->())) {
        super.init(sections: [TableSection(items: items)]) { item, indexPath in
            return CellDescriptor { (cell: Cell) in
                configure(item, cell)
            }
        }
        setupTableView()
    }
    
    init(items: [Item]) {
        super.init(sections: [TableSection(items: items)])
        setupTableView()
    }
    
    fileprivate func setupTableView() {
       
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        super.tableView(tableView, didSelectRowAt: indexPath)
        
        let item = sections[indexPath.section].displayedItems[indexPath.row]
        if let cell: Cell = cell(forIndexPath: indexPath){
            didSelectWithCell?(item, indexPath, cell)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
