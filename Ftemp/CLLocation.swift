//
//  CLLocation.swift
//  Map
//
//  Created by Nabil Muthanna on 2016-11-28.
//  Copyright © 2016 Nabil Muthanna. All rights reserved.
//

import Foundation
import CoreLocation

extension CLLocation {
    
    enum Keys: String { case latitude, longitude }
    
    convenience init?(dictionary: JSONDictionary) {
        guard  let latitude = dictionary[Keys.latitude.rawValue] as? Double,
            let longitude = dictionary[Keys.longitude.rawValue] as? Double else {
                return nil
        }
        
        self.init(latitude: latitude, longitude: longitude)
    }
    
    
    func converToJson() -> JSONDictionary {
        return [
            Keys.latitude.rawValue: coordinate.latitude,
            Keys.longitude.rawValue: coordinate.longitude
        ]
    }
}
