//
//  Comment.swift
//  Ftemp
//
//  Created by Nabil Muthanna on 2017-02-16.
//  Copyright © 2017 Nabil Muthanna. All rights reserved.
//

import PromiseKit
import FirebaseDatabase


struct Comment {

    let key: String
    let body: String
    let postedDate: Date
    let ownerId: String
    let imageUrl: URL?
    var rateMatrix = RaterMatrix()
}



extension Comment {

    var owner: Resource<Owner?> {
        get {
            return Resource<Owner?>(query: EventEndPoints.loadOwner(ownerId).ref, eventType: FIRDataEventType.value) { (snapshot)  in
                return .success(snapshot.item() ?? nil)
            }
        }
    }
    
    var replies: Resource<[Comment]> {
        get {
            return Resource<[Comment]>(query: CommentEndPoints.loadReplies(self).ref, eventType: FIRDataEventType.value) { (snapshot)  in
                return .success(snapshot.items() ?? [])
            }
        }
    }
}

extension Comment: JSONDecodable {
    enum Keys: String { case key, body, details, postedDate,  ownerId, rateMatrix, imageUrl }
    
    init?(_ snapShot: FIRDataSnapshot) {
        guard let dictionary = snapShot.value => JSONDictionary.self,
            let body = dictionary.string(key: Keys.body.rawValue),
            let postedDate = dictionary.dateFromMilliseconds(key: Keys.postedDate.rawValue),
            let ownerId = dictionary.string(key: Keys.ownerId.rawValue) else {
                return nil
        }
        
        self.key = snapShot.key
        self.body = body
        self.postedDate = postedDate
        self.ownerId = ownerId
        self.rateMatrix = dictionary.dictionary(key: Keys.rateMatrix.rawValue, or: RaterMatrix())
        self.imageUrl = dictionary.url(key: Keys.imageUrl.rawValue)
    }
    
    func convertToJson() -> JSONDictionary {
        var data: [String: Any] = [
            Keys.body.rawValue: body,
            Keys.postedDate.rawValue: postedDate.milliSeconds,
            Keys.ownerId.rawValue: ownerId,
            Keys.rateMatrix.rawValue: rateMatrix.convertToJson()
        ]
        imageUrl >>>= { data[Keys.imageUrl.rawValue] = $0.absoluteString }
        
        return data
    }
}

