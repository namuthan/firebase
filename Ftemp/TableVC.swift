//
//  TableVC.swift
//  Ftemp
//
//  Created by Nabil Muthanna on 2017-02-16.
//  Copyright © 2017 Nabil Muthanna. All rights reserved.
//
//33riugz3jjmkhlgxxuzf52w4mlz23vqxwlvil2lg5kjbc3zme5oq
import UIKit
import Firebase
import FirebaseAuth
import PromiseKit

class TableVC: UIViewController {
    
    var user = UserManager.shared.currentUser
    let w = WebService()
    
    var events: [Event] = []
    var loadedFirstPage = false
    
    fileprivate lazy var btn: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Load Next Page", for: .normal)
        btn.setTitleColor(MainStyleSheet.Theme.main.color, for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.addTarget(self, action: #selector(TableVC.loadNextPage), for: .touchUpInside)
        return btn
    }()
    
    func setupUI() {
        view.addSubview(btn)
        btn.center(inView: view)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = MainStyleSheet.Theme.background.color
        setupUI()
        guard let userId = UserManager.shared.currentUserId else { return }

        
        let today = Date()
        let yesterday = Calendar.current.date(byAdding: .day, value: -2, to: today)
        let tomrw = Calendar.current.date(byAdding: .day, value: 1, to: today)
        
        
        // sava event
        let location = Location(key: LocationManager.shared.newId, streetAddress: "sample street address one", placeName: "sample place name", city: "sample city", province: "samole province", country: "sample country", location: nil)
        let interests = [Interest(name: "Sample Interest one")]
        var event = Event(name: "sample Event 1", details: "sampe Details", startDate: yesterday!, endDate: tomrw!, ownerId: userId, locationId: location.key)
        event.interests = interests
        
        event.save(withLocatio: location, completion: { (result) in
            switch result {
            case .success():
                print("success saving the event")
            case .failure(let err):
                print("error saving the event \(err)")
            }
        })
    }
    
    @objc func loadNextPage() {
        
        var lastItemKey: String? = nil
        if events.count > 0 {
            lastItemKey = events[0].key
        }
        let pagedQuery = PagedQuery(lastItemKey: lastItemKey, pageSize: 2)
        Event.load(withQuery: pagedQuery) { (result) in
            switch result {
            case .success(let events):
                print("success loading events \(events)")
                self.events += events
                self.sortEventsByKey()
                
            case .failure(let err):
                print("error loading the events \(err)")
            }
        }

    }
    
    func sortEventsByKey() {
        events.sort { (one, two) -> Bool in
            return one.key < two.key
        }
    }

    
    func documentation () {
        guard let userId = UserManager.shared.currentUserId else { return }
        
            
        let today = Date()
        let yesterday = Calendar.current.date(byAdding: .day, value: -2, to: today)
        let tomrw = Calendar.current.date(byAdding: .day, value: 1, to: today)
        
        // sava event
        let location = Location(key: LocationManager.shared.newId, streetAddress: "sample street address", placeName: "sample place name", city: "sample city", province: "samole province", country: "sample country", location: nil)
        let interests = [Interest(name: "Sample Interest one")]
        var event = Event(name: "sample Event 1", details: "sampe Details", startDate: yesterday!, endDate: tomrw!, ownerId: userId, locationId: location.key)
        event.interests = interests
   
        event.save(withLocatio: location, completion: { (result) in
            switch result {
            case .success():
                print("success saving the event")
            case .failure(let err):
                print("error saving the event \(err)")
            }
        })
        
        
        // paginate reading events - u need to keep a list of all the retreived events
        // It will retrieve from the most recent to the oldest
        var lastItemKey: String? = nil
        if events.count > 0 {
            lastItemKey = events[0].key
        }
        let pagedQuery = PagedQuery(lastItemKey: lastItemKey, pageSize: 2)
        Event.load(withQuery: pagedQuery) { (result) in
            switch result {
            case .success(let events):
                print("success loading events \(events)")
                self.events += events
                self.sortEventsByKey()
                
            case .failure(let err):
                print("error loading the events \(err)")
            }
        }
        
        
        // read events within a range 
        let query = DateRangeQuery(startDate: yesterday!, endDate: tomrw!)
        Event.load(forRange: query) { (result) in
            switch result {
            case .success(let events):
                print("success loading events \(events)")
                self.events += events
                self.sortEventsByKey()
                
            case .failure(let err):
                print("error loading the events \(err)")
            }
        }
        
        
        //read events for user 
        Event.load(forUser: userId, completion: { (events) in
            print(events)
        })
    }
}















