//
//  EventEndPoints.swift
//  Ftemp
//
//  Created by Nabil Muthanna on 2017-03-26.
//  Copyright © 2017 Nabil Muthanna. All rights reserved.
//

import FirebaseDatabase


enum EventEndPoints: EndPoint {
    
    case load()
    case loadWithKey(String)
    case loadLocation(String)
    case loadOwner(String)
    case loadComments(Event)
    
    case save(Event)
    case saveComment(Comment, Event)
    case saveGuest(Guest, Event)
    case saveLocation(Location)
    
    
    var rootRef: FIRDatabaseReference {
        get {
            return self.root.child(Briotie.RootKeys.events.rawValue)
        }
    }
    
    var ref: FIRDatabaseReference {
        get {
            switch self {
                
            case .load():
                return rootRef
            case .loadWithKey(let key):
                return rootRef.child(key)
            case .loadLocation(let locationId):
                return rootRef.child(Briotie.RootKeys.locations.rawValue).child(locationId)
            case .loadOwner(let userId):
                return root.child(Briotie.RootKeys.users.rawValue).child(userId)
            case .loadComments(let event):
                return root.child(Briotie.RootKeys.eventComments.rawValue).child(event.key)
                
            case .save(let event):
                return rootRef.child(event.key)
                
            case .saveComment(let comment, let event):
                return root.child(Briotie.RootKeys.eventComments.rawValue).child(event.key).child(comment.key)
                
            case .saveGuest(let guest, let event):
                return root.child(Briotie.RootKeys.guests.rawValue).child(event.key).child(guest.key)
                
            case .saveLocation(let location):
                return LocationEndPoints.save(location).ref
            }
        }
    }
}
