//
//  Discussion.swift
//  Ftemp
//
//  Created by Nabil Muthanna on 2017-02-16.
//  Copyright © 2017 Nabil Muthanna. All rights reserved.
//

import PromiseKit
import FirebaseDatabase

struct Discussion {
    
    let key: String
    let title: String
    let body: String
    let postedDate: Date
    var ownerId: String //current user id

    var rateMatrix = RaterMatrix()
    var interests: [Interest] = []
    
    let imageUrl: URL?
}

extension Discussion {
    
    static var newKey: String {
        get {
            return DiscussionEndPoints.load().ref.childByAutoId().key
        }
    }
    
    var owner: Resource<Owner?> {
        get {
            return Resource<Owner?>(query: DiscussionEndPoints.loadOwner(ownerId).ref, eventType: FIRDataEventType.value) { (snapshot)  in
                return .success(snapshot.item() ?? nil)
            }
        }
    }
    
    var comments: Resource<[Comment]> {
        get {
            return Resource<[Comment]>(query: DiscussionEndPoints.loadComments(self).ref, eventType: FIRDataEventType.value) { (snapshot)  in
                return .success(snapshot.items() ?? [])
            }
        }
    }

}

// MARK: - JSONDecodable

extension Discussion: JSONDecodable {
    enum Keys: String { case key, title, body, postedDate,  ownerId, imageUrl, rateMatrix, interests }
    
    init?(_ snapShot: FIRDataSnapshot) {
        guard let dictionary = snapShot.value => JSONDictionary.self,
            let title = dictionary.string(key: Keys.title.rawValue),
            let body = dictionary.string(key: Keys.body.rawValue),
            let postedDate = dictionary.dateFromMilliseconds(key: Keys.postedDate.rawValue),
            let ownerId = dictionary.string(key: Keys.ownerId.rawValue) else {
                return nil
        }
        
        self.title = title
        self.body = body
        self.postedDate = postedDate
        self.ownerId = ownerId
        self.imageUrl = dictionary.url(key: Keys.imageUrl.rawValue)
        self.interests = dictionary.array(key: Keys.interests.rawValue) ?? []
        self.rateMatrix = dictionary.dictionary(key: Keys.rateMatrix.rawValue, or: RaterMatrix())
        self.key = snapShot.key
        
    }
    
    func convertToJson() -> JSONDictionary {
        
        var data: [String: Any] =  [
            Keys.title.rawValue: title,
            Keys.body.rawValue: body,
            Keys.postedDate.rawValue: postedDate.milliSeconds,
            Keys.ownerId.rawValue: ownerId,
            Keys.rateMatrix.rawValue: rateMatrix.convertToJson(),
            Keys.interests.rawValue: interests.map({return $0.convertToJson()})
        ]
        imageUrl >>>= { data[Keys.imageUrl.rawValue] = $0.absoluteString }
     
        return data
    }
    
}


// MARK: -  Read

extension Discussion {
    
    static func load(withQuery query: PagedQuery, completion: @escaping ((Result<[Discussion]>) -> Void)) {
        
        if let lastItemKey = query.lastItemKey {
            DiscussionEndPoints.load().ref.queryOrderedByKey().queryLimited(toLast: UInt(query.pageSize + 1)).queryEnding(atValue: lastItemKey).observe(.value, with: { (snapshot) in
                var items: [Discussion] = snapshot.items() ?? []
                if items.count > 0 {
                    items.remove(at: items.count - 1)
                }
                WebService.shared.dispatchToMainThread(.success(items), completion: completion)
            })
        } else {
            DiscussionEndPoints.load().ref.queryOrderedByKey().queryLimited(toLast: UInt(query.pageSize)).observe(.value, with: { (snapshot) in
                let items: [Discussion] = snapshot.items() ?? []
                WebService.shared.dispatchToMainThread(.success(items), completion: completion)
            })
        }
    }
    
    static func load(forRange range: DateRangeQuery, completion: @escaping ((Result<[Discussion]>) -> Void)) {
        DiscussionEndPoints.load().ref.queryOrdered(byChild: Discussion.Keys.postedDate.rawValue).queryStarting(atValue: range.startDate.milliSeconds).queryEnding(atValue: range.endDate.milliSeconds).observe(.value, with: { (snapshot) in
            let items: [Discussion] = snapshot.items() ?? []
            WebService.shared.dispatchToMainThread(.success(items), completion: completion)
        })
    }
    
    static func load(WithKey key: String,  completion: @escaping ((Result<Discussion?>) -> Void)){
        DiscussionEndPoints.load().ref.child(key).observe(.value, with: { (snapshot) in
            let item: Discussion? = snapshot.item()
            WebService.shared.dispatchToMainThread(.success(item), completion: completion)
        })
    }
    
    static func load(forUser userId: String, completion: @escaping ((Result<[Discussion]>) -> Void)) {
        DiscussionEndPoints.load().ref.queryOrdered(byChild: Discussion.Keys.ownerId.rawValue).queryEqual(toValue: userId).observe(.value, with: { (snapshot) in
            let items: [Discussion] = snapshot.items() ?? []
            WebService.shared.dispatchToMainThread(.success(items), completion: completion)
        })
    }
    
    
    func loadComments(withQuery query: PagedQuery, completion: @escaping ((Result<[Comment]>) -> Void)) {
        
        if let lastItemKey = query.lastItemKey {
            DiscussionEndPoints.loadComments(self).ref.queryOrderedByKey().queryLimited(toLast: UInt(query.pageSize + 1)).queryEnding(atValue: lastItemKey).observe(.value, with: { (snapshot) in
                var items: [Comment] = snapshot.items() ?? []
                if items.count > 0 {
                    items.remove(at: items.count - 1)
                }
                WebService.shared.dispatchToMainThread(.success(items), completion: completion)
            })
        } else {
            DiscussionEndPoints.loadComments(self).ref.queryOrderedByKey().queryLimited(toLast: UInt(query.pageSize)).observe(.value, with: { (snapshot) in
                let items: [Comment] = snapshot.items() ?? []
                WebService.shared.dispatchToMainThread(.success(items), completion: completion)
            })
        }
    }

}


// MARK: Creating

extension Discussion {
    
    
    // MARK: - Create
    
    func save(completion: @escaping ((Result<Void>) -> Void)) {
        
        let discussionRef = DiscussionEndPoints.save(self).ref
        discussionRef.updateChildValues(self.convertToJson()) { (err: Error?, ref: FIRDatabaseReference) in
            if let error = err {
                completion(.failure(error))
            } else {
                completion(.success())
            }
        }
    }
    
    func append(comment: Comment, completion: @escaping ((Result<Void>) -> Void)) {
        DiscussionEndPoints.saveComment(comment, self).ref.updateChildValues(comment.convertToJson()) { (err: Error?, ref: FIRDatabaseReference) in
            if let error = err {
                completion(.failure(error))
            } else {
                completion(.success())
            }
        }
    }
}


