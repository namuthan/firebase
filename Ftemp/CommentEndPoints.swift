//
//  CommentEndPoints.swift
//  Ftemp
//
//  Created by Nabil Muthanna on 2017-03-26.
//  Copyright © 2017 Nabil Muthanna. All rights reserved.
//

import FirebaseDatabase


enum CommentEndPoints: EndPoint {
    
    case load()
    case loadReplies(Comment)
    case loadOwner(String)

    case saveReply(Comment)
    
    var ref: FIRDatabaseReference {
        get {
            switch self {
            case .load():
                return root.child(Briotie.RootKeys.commentReplies.rawValue)
            case .loadReplies(let comment):
                return root.child(Briotie.RootKeys.commentReplies.rawValue).child(comment.key)
            case .loadOwner(let ownerId):
                return root.child(Briotie.RootKeys.users.rawValue).child(ownerId)
                
            case .saveReply(let reply):
                return root.child(Briotie.RootKeys.commentReplies.rawValue).child(reply.key)
            }
        }
    }
}
