//
//  InterestManager.swift
//  Ftemp
//
//  Created by Nabil Muthanna on 2017-03-03.
//  Copyright © 2017 Nabil Muthanna. All rights reserved.
//

import Foundation


class InterestManager {
    
    static let shared = InterestManager()
    
    
    var newId: String {
        get {
            return InterestEndPoints.load.ref.childByAutoId().key
        }
    }
}
