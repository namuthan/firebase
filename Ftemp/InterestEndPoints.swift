//
//  InterestEndPoints.swift
//  Ftemp
//
//  Created by Nabil Muthanna on 2017-03-26.
//  Copyright © 2017 Nabil Muthanna. All rights reserved.
//

import FirebaseDatabase


enum InterestEndPoints: EndPoint {
    
    var rootRef: FIRDatabaseReference {
        get {
            return self.root.child(Briotie.RootKeys.interests.rawValue)
        }
    }
    
    case load
    
    case save(Interest)
    case saveEvent(Event, Interest)
    
    var ref: FIRDatabaseReference {
        get {
            switch self {
            case .load():
                return rootRef
            case .save(let interest):
                return rootRef.child(interest.key)
            case .saveEvent(let event, let interest):
                return root.child(Briotie.RootKeys.interestEvents.rawValue).child(interest.key).child(event.key)
            }
        }
    }

    
}
