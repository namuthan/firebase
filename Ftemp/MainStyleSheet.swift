//
//  StyleSheet.swift
//  X4DWorkPackages
//
//  Created by Nabil Muthanna on 2016-12-21.
//  Copyright © 2016 Nabil Muthanna. All rights reserved.
//

import UIKit


struct MainStyleSheet {
    
    enum Theme {
        case main, background, contentBackground
        
        var color: UIColor {
            switch self {
            case .main: return UIColor.rgb(r: 255, g: 71, b: 0)
            case .background: return UIColor.rgb(r: 240, g: 240, b: 240)
            case .contentBackground: return UIColor.white
            }
        }
    }

    enum Text {
        case primary, secondary
        
        var color: UIColor {
            switch self {
            case .primary: return UIColor.black
            case .secondary: return .gray
            }
        }
    }
    
    enum Font {
        case header, subHeader, detail
        
        var font: UIFont {
            switch self {
            case .header: return UIFont.systemFont(ofSize: 14)
            case .subHeader: return UIFont.systemFont(ofSize: 10)
            case .detail: return UIFont.systemFont(ofSize: 12)
            }
        }
    }
    
    
    enum BriotieDate  {
        case main
        
        var formatter: DateFormatter {
            switch self {
            case .main:
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
                return formatter
            }
        }
    }
}


