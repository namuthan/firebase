//
//  UserManager.swift
//  Ftemp
//
//  Created by Nabil Muthanna on 2017-03-03.
//  Copyright © 2017 Nabil Muthanna. All rights reserved.
//

import FirebaseAuth
import FirebaseDatabase
import PromiseKit

class UserManager {
    
    
    // login / create account / current user
    
    static let shared = UserManager()
    
    
    // MARK: - Authentication
    
    var isThereACurrentUser: Bool {
        get {
            return FIRAuth.auth()?.currentUser?.email != nil ? true : false
        }
    }
    
    var currentUserId: String? {
        get {
            return FIRAuth.auth()?.currentUser?.uid
        }
    }
    
    var currentUserEmail: String? {
        get {
            return FIRAuth.auth()?.currentUser?.email
        }
    }
    
    
    var currentUser: Promise<User>? {
        get {
            guard let _ = FIRAuth.auth()?.currentUser?.email,
                let uid = FIRAuth.auth()?.currentUser?.uid else { return nil }
            let res = Resource<User>(query: UserEndPoints.loadWithKey(uid).ref, eventType: .value) { (snapshot) -> Result<User> in
                
                guard let user = User(snapshot) else {
                    let err = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Not Valid User"])
                    return Result.failure(err)
                }
                return .success(user)
            }
            return WebService.shared.perform(res)
        }
    }
   
    func signIn(withEmail email: String, password: String, completion: @escaping (Result<Bool>)->()) {
        FIRAuth.auth()?.signIn(withEmail: email, password: password, completion: { [weak self] (user, error) in
            guard self != nil else { return }
            if let error = error {
                completion(.failure(error))
                return
            }
            completion(.success(true))
        })
    }
    
  
    func createAccount(forUser briotieUser: User, withPassword password: String, completion: @escaping ((Result<User>) ->())) {
        FIRAuth.auth()?.createUser(withEmail: briotieUser.email, password: password, completion: { [weak self] (user, err) in
            guard let strongSelf = self else { return }
            if let err = err {
                completion(.failure(err))
                return
            }
            
            // successfully created the user
           strongSelf.save(user: briotieUser, completion: completion)
        })
    }
  
    func signOutCurrentUser() {
        try? FIRAuth.auth()?.signOut()
    }
    
    // MARK: - Database
    
    func save(user: User, completion: @escaping (Result<User>)->()?) {
        
        let data = [user.email: user.convertToJson()]
        let ref = UserEndPoints.save().ref
        ref.updateChildValues(data, withCompletionBlock: { [weak self] (error, ref) in
            guard self != nil else { return }
            if let error = error {
                completion(.failure(error))
                return
            }
            completion(.success(user))
        })
    }
}
