//
//  User.swift
//  Ftemp
//
//  Created by Nabil Muthanna on 2017-02-16.
//  Copyright © 2017 Nabil Muthanna. All rights reserved.
//

import Foundation
import FirebaseDatabase
import PromiseKit

// MARK: - User

struct User {
    
    let uid: String
    let email: String
    let username: String
    let fullName: String
    let bio: String?
    let profileImageUrl: URL?
    
}

extension User {
    
    var followers: Resource<[User]> {
        get {
            return Resource<[User]>(query: UserEndPoints.loadFollowers(email).ref, eventType: .value) { (snapshot)  in
                return .success(snapshot.items() ?? [])
            }
        }
    }
    
    var followings: Resource<[User]> {
        get {
            return Resource<[User]>(query: UserEndPoints.loadFollowings(email).ref, eventType: .value) { (snapshot)  in
                return .success(snapshot.items() ?? [])
            }
        }
    }

    var subscriptions: Resource<[Interest]> {
        get {
            return Resource<[Interest]>(query: UserEndPoints.loadSubScriptions(email).ref, eventType: .value) { (snapshot)  in
                return .success(snapshot.items() ?? [])
            }
        }
    }
    
}


// MARK: - JSONDecodable

extension User: JSONDecodable {
    
    enum Keys: String { case uid, email, username, fullName, bio, profileImageUrl }
    
    init?(_ snapShot: FIRDataSnapshot) {
        guard let dictionary = snapShot.value => JSONDictionary.self,
            let username = dictionary.string(key: Keys.username.rawValue),
            let fullName = dictionary.string(key: Keys.fullName.rawValue) else {
            return nil
        }
        
        self.uid = snapShot.key
        self.email = snapShot.key
        self.username = username
        self.fullName = fullName
        self.bio =  dictionary.string(key: Keys.bio.rawValue)
        self.profileImageUrl = dictionary.url(key: Keys.profileImageUrl.rawValue)
    }

    func convertToJson() -> JSONDictionary {
        var data = [
            Keys.email.rawValue: email,
            Keys.username.rawValue: username,
            Keys.fullName.rawValue: fullName
        ]
        
        bio >>>= { data[Keys.bio.rawValue] = $0 }
        profileImageUrl >>>= { data[Keys.profileImageUrl.rawValue] = $0.absoluteString }
        return data
    }
}








