//
//  Event.swift
//  Ftemp
//
//  Created by Nabil Muthanna on 2017-02-16.
//  Copyright © 2017 Nabil Muthanna. All rights reserved.
//

import CoreLocation
import PromiseKit
import FirebaseDatabase

struct Event {
    
    let key: String
    let name: String
    let details: String
    let startDate: Date
    let endDate: Date
    let isPrivate: Bool
    let ownerId: String //current user id
    let locationId: String

    var raterMatrix = RaterMatrix()
    var interests: [Interest] = []
    
    var guestLimit: Int? = nil
    var imageUrl: URL? = nil

}


extension Event {
    
    init(name: String, details: String, startDate: Date, endDate: Date, isPrivate: Bool = true, ownerId: String, locationId: String) {
        self.name = name
        self.details = details
        self.startDate = startDate
        self.endDate = endDate
        self.isPrivate = isPrivate
        self.ownerId = ownerId
        self.locationId = locationId
        self.key = Event.newKey
    }
}

// MARK: - Computed Properties

extension Event {
    
    static var newKey: String {
        get {
            return EventEndPoints.load().ref.childByAutoId().key
        }
    }
    
    var owner: Resource<Owner?> {
        get {
            return Resource<Owner?>(query: EventEndPoints.loadOwner(ownerId).ref, eventType: FIRDataEventType.value) { (snapshot)  in
                return .success(snapshot.item() ?? nil)
            }
        }
    }
   
    var location: Resource<Location?> {
        return Resource<Location?>(query: EventEndPoints.loadLocation(key).ref, eventType: FIRDataEventType.value) { (snapshot)  in
                return .success(snapshot.item() ?? nil)
            }
    }
    
    var commmets: Resource<[Comment]> {
        get {
            return Resource<[Comment]>(query: EventEndPoints.loadComments(self).ref, eventType: .value) { (snapshot)  in
                return .success(snapshot.items() ?? [])
            }
        }
    }
    
    // TODO: - add guest computed resource
    
}

extension Event: JSONDecodable {
    enum Keys: String { case key, name, details, startDate, endDate, ownerId, isPrivate, locationId,
        guestLimit, imageUrl,
        interests, raterMatrix
    }
    
    init?(_ snapShot: FIRDataSnapshot) {
        guard let dictionary = snapShot.value => JSONDictionary.self,
            let name = dictionary.string(key: Keys.name.rawValue),
            let details = dictionary.string(key: Keys.details.rawValue),
            let startDate = dictionary.dateFromMilliseconds(key: Keys.startDate.rawValue),
            let endDate = dictionary.dateFromMilliseconds(key: Keys.endDate.rawValue),
            let ownerId = dictionary.string(key: Keys.ownerId.rawValue),
            let isPrivate = dictionary.bool(key: Keys.isPrivate.rawValue),
            let locationId = dictionary.string(key: Keys.locationId.rawValue) else {
                return nil
        }
        
        self.key = snapShot.key
        self.name = name
        self.details = details
        self.startDate = startDate
        self.endDate = endDate
        self.ownerId = ownerId
        self.isPrivate = isPrivate
        self.locationId = locationId
        
        self.raterMatrix = dictionary.dictionary(key: Keys.raterMatrix.rawValue, or: RaterMatrix())
        
        //read interests
        let interestsDict: [String: String]? = dictionary.dictionary(key: Keys.interests.rawValue) as? [String : String]
        if let interestsDict = interestsDict {
            for (key, value) in interestsDict {
                self.interests.append(Interest(key: key, name: value))
            }
        }
        
        self.guestLimit = dictionary.int(key: Keys.guestLimit.rawValue)
        self.imageUrl = dictionary.url(key: Keys.imageUrl.rawValue)
    }
    
    
    func convertToJson() -> JSONDictionary {
        var data: [String: Any] =  [
            Keys.name.rawValue: name,
            Keys.details.rawValue: details,
            Keys.startDate.rawValue: startDate.milliSeconds,
            Keys.endDate.rawValue: endDate.milliSeconds,
            Keys.ownerId.rawValue: ownerId,
            Keys.isPrivate.rawValue: isPrivate,
            Keys.locationId.rawValue: locationId,
            
            Keys.raterMatrix.rawValue: raterMatrix.convertToJson(),
        ]
        guestLimit >>>= { data[Keys.guestLimit.rawValue] = $0 }
        imageUrl >>>= { data[Keys.imageUrl.rawValue] = $0.absoluteString }
        
        
        var interestsDict = [String: Any]()
        for interest in interests {
            interestsDict[interest.key] = interest.name
        }
        data[Keys.interests.rawValue] = interestsDict
        return data
    }
}


// MARK: - Reading

extension Event {
        
    static func load(withQuery query: PagedQuery, completion: @escaping ((Result<[Event]>) -> Void)) {
        
        if let lastItemKey = query.lastItemKey {
            EventEndPoints.load().ref.queryOrderedByKey().queryLimited(toLast: UInt(query.pageSize + 1)).queryEnding(atValue: lastItemKey).observe(.value, with: { (snapshot) in
                var items: [Event] = snapshot.items() ?? []
                if items.count > 0 {
                    items.remove(at: items.count - 1)
                }
                WebService.shared.dispatchToMainThread(.success(items), completion: completion)
            })
        } else {
            EventEndPoints.load().ref.queryOrderedByKey().queryLimited(toLast: UInt(query.pageSize)).observe(.value, with: { (snapshot) in
                let items: [Event] = snapshot.items() ?? []
                WebService.shared.dispatchToMainThread(.success(items), completion: completion)
            })
        }
    }
    
    static func load(forRange range: DateRangeQuery, completion: @escaping ((Result<[Event]>) -> Void)) {
        EventEndPoints.load().ref.queryOrdered(byChild: Event.Keys.startDate.rawValue).queryStarting(atValue: range.startDate.milliSeconds).queryEnding(atValue: range.endDate.milliSeconds).observe(.value, with: { (snapshot) in
            let items: [Event] = snapshot.items() ?? []
            WebService.shared.dispatchToMainThread(.success(items), completion: completion)
        })
    }
    
    static func loadEvent(key: String,  completion: @escaping ((Result<Event?>) -> Void)){
        EventEndPoints.load().ref.child(key).observe(.value, with: { (snapshot) in
            let item: Event? = snapshot.item()
            WebService.shared.dispatchToMainThread(.success(item), completion: completion)
        })
    }
    
    static func load(forUser userId: String, completion: @escaping ((Result<[Event]>) -> Void)) {
        EventEndPoints.load().ref.queryOrdered(byChild: Event.Keys.ownerId.rawValue).queryEqual(toValue: userId).observe(.value, with: { (snapshot) in
            let items: [Event] = snapshot.items() ?? []
            WebService.shared.dispatchToMainThread(.success(items), completion: completion)
        })
    }
    
    
    func loadComments(withQuery query: PagedQuery, completion: @escaping ((Result<[Comment]>) -> Void)) {
        
        if let lastItemKey = query.lastItemKey {
            EventEndPoints.loadComments(self).ref.queryOrderedByKey().queryLimited(toLast: UInt(query.pageSize + 1)).queryEnding(atValue: lastItemKey).observe(.value, with: { (snapshot) in
                var items: [Comment] = snapshot.items() ?? []
                if items.count > 0 {
                    items.remove(at: items.count - 1)
                }
                WebService.shared.dispatchToMainThread(.success(items), completion: completion)
            })
        } else {
            EventEndPoints.loadComments(self).ref.queryOrderedByKey().queryLimited(toLast: UInt(query.pageSize)).observe(.value, with: { (snapshot) in
                let items: [Comment] = snapshot.items() ?? []
                WebService.shared.dispatchToMainThread(.success(items), completion: completion)
            })
        }
    }
    
}

// MARK: Creating

extension Event {
    
    
    // MARK: - Create
    
    func save(withLocatio location: Location, completion: @escaping ((Result<Void>) -> Void)) {
        
        let eventRef = EventEndPoints.save(self).ref
        let locationRef = EventEndPoints.saveLocation(location).ref
        
        let data = [eventRef.extendedPath(to: Briotie.root): self.convertToJson(), locationRef.extendedPath(to: Briotie.root): location.convertToJson()]
        
        Briotie.root.updateChildValues(data) { (err: Error?, ref: FIRDatabaseReference) in
            if let error = err {
                completion(.failure(error))
            } else {
                completion(.success())
            }
        }
    }
    
    func append(comment: Comment, completion: @escaping ((Result<Void>) -> Void)) {
        EventEndPoints.saveComment(comment, self).ref.updateChildValues(comment.convertToJson()) { (err: Error?, ref: FIRDatabaseReference) in
            if let error = err {
                completion(.failure(error))
            } else {
                completion(.success())
            }
        }
    }
    
    func append(guest: Guest, completion: @escaping ((Result<Void>) -> Void)) {
        EventEndPoints.saveGuest(guest, self).ref.updateChildValues(guest.convertToJson()) { (err: Error?, ref: FIRDatabaseReference) in
            if let error = err {
                completion(.failure(error))
            } else {
                completion(.success())
            }
        }
    }
}






