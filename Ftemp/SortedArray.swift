//
//  SortedArray.swift
//  X4D
//
//  Created by Nabil Muthanna on 2017-02-03.
//  Copyright © 2017 Nabil Muthanna. All rights reserved.
//

import Foundation


struct SortedArray<Element> {
    
    fileprivate var elements: [Element]
    let areInIncreasingOrder: (Element, Element) -> Bool
    
    init<S: Sequence>(unsorted: S, areInIncreasingOrder: @escaping (Element, Element) -> Bool) where S.Iterator.Element == Element {
        elements = unsorted.sorted(by: areInIncreasingOrder)
        self.areInIncreasingOrder = areInIncreasingOrder
    }
    
    mutating func insert(_ element: Element) {
        elements.append(element)
        _ = elements.sorted(by: areInIncreasingOrder)
    }
}

extension SortedArray: Collection {
    
    var startIndex: Int {
        return elements.startIndex
    }
    
    var endIndex: Int {
        return elements.endIndex
    }

    subscript(index: Int) -> Element {
        return elements[index]
    }
    
    func index(after i: Int) -> Int {
        return elements.index(after: i)
    }
    
    func min() -> Element? {
        return elements.first
    }
    
    func max() -> Element? {
        return elements.last
    }
    
}


extension SortedArray where Element: Comparable {
    
    init<S: Sequence>(unsorted: S) where S.Iterator.Element == Element {
        self.init(unsorted: unsorted, areInIncreasingOrder: <)
    }
}


