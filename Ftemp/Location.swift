//
//  Location.swift
//  Ftemp
//
//  Created by Nabil Muthanna on 2017-02-16.
//  Copyright © 2017 Nabil Muthanna. All rights reserved.
//

import Foundation
import CoreLocation
import FirebaseDatabase

struct Location {
   
    let key: String
    let streetAddress: String
    let placeName: String
    let city: String?
    let province: String?
    let country: String?
    let location: CLLocation?
}


extension Location: JSONDecodable {
    enum Keys: String { case key, location, placeName, streetAddress, city, province, country}

    init?(_ snapShot: FIRDataSnapshot) {
        guard let dictionary = snapShot.value => JSONDictionary.self,
            let streetAddress = dictionary.string(key: Keys.streetAddress.rawValue),
            let placeName = dictionary.string(key: Keys.placeName.rawValue) else {
                return nil
        }
        
        self.key = snapShot.key
        self.streetAddress = streetAddress
        self.placeName = placeName
        self.location = dictionary.dictionary(key: Keys.location.rawValue) >>>= { CLLocation(dictionary: $0) }
        city = dictionary.string(key: Keys.city.rawValue)
        province = dictionary.string(key: Keys.province.rawValue)
        country = dictionary.string(key: Keys.country.rawValue)
    }
    
    
    func convertToJson() -> JSONDictionary {
       
        var data: [String: Any] = [
            Keys.streetAddress.rawValue: streetAddress
        ]
        city >>>= { data[Keys.city.rawValue] = $0 }
        province >>>= { data[Keys.province.rawValue] = $0 }
        country >>>= { data[Keys.country.rawValue] = $0 }
        location >>>= { data[Keys.location.rawValue] = $0.converToJson() }

        return data
    }
}

