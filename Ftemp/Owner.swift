//
//  Owner.swift
//  Ftemp
//
//  Created by Nabil Muthanna on 2017-02-16.
//  Copyright © 2017 Nabil Muthanna. All rights reserved.
//

import Foundation

struct Owner {
    
    let email: String
    let userName: String
    let profileImageUrl: URL?
    
}

extension Owner: JSONDecodable {
    enum Keys: String { case email, userName,  profileImageUrl }
    
    init?(_ dictionary: JSONDictionary) {
        guard let email = dictionary.string(key: Keys.email.rawValue),
            let userName = dictionary.string(key: Keys.userName.rawValue) else {
                return nil
        }
        
        self.email = email
        self.userName = userName
        self.profileImageUrl = dictionary.url(key: Keys.profileImageUrl.rawValue)
    }
    
    func convertToJson() -> JSONDictionary {
        var data = [
            Keys.email.rawValue: email,
            Keys.userName.rawValue: userName,
        ]
        profileImageUrl >>>= { data[Keys.profileImageUrl.rawValue] = $0.absoluteString }
        return data
    }
}
