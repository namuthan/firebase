//
//  Array.swift
//  X4DWorkPackages
//
//  Created by Nabil Muthanna on 2016-12-14.
//  Copyright © 2016 Nabil Muthanna. All rights reserved.
//

import Foundation

protocol Summable {
    static var Zero: Self { get }
    static func +(lhs: Self, rhs: Self) -> Self
}

extension Int: Summable {
    static var Zero: Int { return 0 }
}
extension Double: Summable {
    static var Zero: Double { return 0.0 }
}
extension Float: Summable {
    static var Zero: Float { return Float(0) }
}


extension Array {
    
    var isNotEmpty: Bool {
        get {
            return !isEmpty
        }
    }
    
    func at(index: Int) -> Element? {
        guard count > index else { return nil }
        return self[index]
    }
}

extension Array where Element: Summable {
    
    func sum() -> Element {
        let sum = self.reduce(Generator.Element.Zero, +)
        return sum
    }
}





extension Collection where Index == Int {
    
    func split(batchSize: Int) -> [SubSequence] {
        
        var result: [SubSequence] = []
        for idx in stride(from: startIndex, to: endIndex, by: batchSize) {
            let end = Swift.min(idx + batchSize, endIndex)
            result.append(self[idx..<end])
        }
        return result
    }
    
}






