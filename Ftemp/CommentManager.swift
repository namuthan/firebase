//
//  CommentManager.swift
//  Ftemp
//
//  Created by Nabil Muthanna on 2017-03-26.
//  Copyright © 2017 Nabil Muthanna. All rights reserved.
//

import Foundation


class CommentManager {
    
    
    static let shared = CommentManager()
    
    // MARK: - Computed Propeties
    
    var newId: String {
        get {
            return CommentEndPoints.load().ref.childByAutoId().key
        }
    }
}
