//
//  MediaPickerManager.swift
//  FaceSnap
//
//  Created by Nabil Muthanna on 2016-09-23.
//  Copyright © 2016 Nabil Muthanna. All rights reserved.
//

// used dependency injection to present the imagePickerController (presentingViewController)

protocol MediaPickerManagerDelegate: class {
    func mediaPickerManager(_ manager: MediaPickerManager, didFinishPickingImage image: UIImage)
}


import UIKit
import MobileCoreServices

class MediaPickerManager: NSObject {
    
    fileprivate let imagePickerController = UIImagePickerController()
    fileprivate weak var presentingViewController: UIViewController?
    
    weak var delegate: MediaPickerManagerDelegate?
    
    init(presentingViewController: UIViewController) {
        self.presentingViewController = presentingViewController
        super.init()
        imagePickerController.delegate = self
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePickerController.sourceType = .camera
            imagePickerController.cameraDevice = .rear
            imagePickerController.mediaTypes = [kUTTypeImage as String]
        } else {
            imagePickerController.sourceType = .photoLibrary
        }
    }
    
    
    func presentImagePickerController(_ animated: Bool) {
        presentingViewController?.present(imagePickerController, animated: animated, completion: nil)
    }
    
    func dismissImagePickerController(_ animated: Bool, completion: @escaping (() -> Void)) {
        imagePickerController.dismiss(animated: animated, completion: completion)
    }
    
}


extension MediaPickerManager: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        delegate?.mediaPickerManager(self, didFinishPickingImage: image)
    }
}



