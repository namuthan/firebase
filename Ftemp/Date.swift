//
//  Date.swift
//  Ftemp
//
//  Created by Nabil Muthanna on 2017-03-03.
//  Copyright © 2017 Nabil Muthanna. All rights reserved.
//

import Foundation


extension Date {
    
    var milliSeconds: Double {
        get {
            return timeIntervalSince1970 * 1000
        }
    }
    
    static func date(fromMilliseconds milliSeconds: Double) -> Date {
        return Date(timeIntervalSince1970: TimeInterval(milliSeconds/1000))
    }
    
}
