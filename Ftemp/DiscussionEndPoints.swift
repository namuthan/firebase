//
//  DiscussionEndPoints.swift
//  Ftemp
//
//  Created by Nabil Muthanna on 2017-03-26.
//  Copyright © 2017 Nabil Muthanna. All rights reserved.
//

import FirebaseDatabase

enum DiscussionEndPoints: EndPoint {
    
    case load()
    case loadWithKey(String)
    case loadOwner(String)
    case loadComments(Discussion)
    
    case save(Discussion)
    case saveComment(Comment, Discussion)

    var rootRef: FIRDatabaseReference {
        get {
            return self.root.child(Briotie.RootKeys.discussions.rawValue)
        }
    }
    
    
    var ref: FIRDatabaseReference {
        get {
            switch self {
            case .load():
                return rootRef
            case .loadWithKey(let key):
                return rootRef.child(key)
            case .loadOwner(let userId):
                return root.child(Briotie.RootKeys.users.rawValue).child(userId)
            case .loadComments(let discussion):
                return root.child(Briotie.RootKeys.discussionComments.rawValue).child(discussion.key)
                
            case .save(let discussion):
                return rootRef.child(discussion.key)
                
            case .saveComment(let comment, let discussion):
                return root.child(Briotie.RootKeys.discussionComments.rawValue).child(discussion.key).child(comment.key)
                
            }
        }
    }
}
    
