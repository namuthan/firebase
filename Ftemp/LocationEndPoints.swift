//
//  LocationEndPoints.swift
//  Ftemp
//
//  Created by Nabil Muthanna on 2017-03-26.
//  Copyright © 2017 Nabil Muthanna. All rights reserved.
//

import FirebaseDatabase


enum LocationEndPoints: EndPoint {
    
    //root ref
    var rootRef: FIRDatabaseReference {
        get {
            return self.root.child(Briotie.RootKeys.locations.rawValue)
        }
    }
    
    // cases
    case load()
    case loadWithKey(String)
    case save(Location)
    
    //ref
    var ref: FIRDatabaseReference {
        get {
            switch self {
            case .load():
                return rootRef
            case .loadWithKey(let key):
                return rootRef.child(key)
            case .save(let location):
                return rootRef.child(location.key)
            }
        }
    }
}
    
