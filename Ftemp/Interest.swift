//
//  Interest.swift
//  Ftemp
//
//  Created by Nabil Muthanna on 2017-02-16.
//  Copyright © 2017 Nabil Muthanna. All rights reserved.
//

import FirebaseDatabase
import PromiseKit

// Location Quering, combine Queries, Read Time updates

struct Interest {
    
    let key: String
    let name: String
    var description: String? = nil
    var imageUrl: URL? = nil
    
}

extension Interest {
    init(name: String) {
        key = InterestManager.shared.newId
        self.name = name
    }
    
    init(key: String, name: String) {
        self.key = key
        self.name = name
    }
}

// MARK: Calculated Properties(fetched from firebase)

extension Interest {
    //events, discussions, users
    
    
    
//    var events: Promise<[Event]> {
//        get {
//            let resource = Resource<Event>(ref: BriotieEndPoints.Interest.loadEvents(key).ref, eventType: EventType.valueOnce, query: nil)
//            return WebService.shared.loadList(resource)
//        }
//    }
//    
//    var discussions: Promise<[Discussion]> {
//        get {
//            let resource = Resource<Discussion>(ref: BriotieEndPoints.Interest.loadDiscussions(key).ref, eventType: EventType.valueOnce, query: nil)
//            return WebService.shared.loadList(resource)
//        }
//    }
//    
//    var subscribers: Promise<[User]> {
//        get {
//            let resource = Resource<User>(ref: BriotieEndPoints.Interest.loadUsers(key).ref, eventType: EventType.valueOnce, query: nil)
//            return WebService.shared.loadList(resource)
//        }
//    }
}

// MARK: - JSONDecodable

extension Interest: JSONDecodable {
    enum Keys: String { case key, name, description, imageUrl, events, discussions, users }
    
    init?(_ snapShot: FIRDataSnapshot) {
        
        guard let dictionary = snapShot.value => JSONDictionary.self,
            let name = dictionary.string(key: Keys.name.rawValue) else {
                
                return nil
        }
        
        self.key = snapShot.key
        self.name = name
        self.description = dictionary.string(key: Keys.description.rawValue)
        self.imageUrl = dictionary.url(key: Keys.imageUrl.rawValue)
    }
    
    func convertToJson() -> JSONDictionary {
        var dict = [
            Keys.name.rawValue: name,
        ]
        description >>>= { dict[Keys.description.rawValue] = $0 }
        imageUrl >>>= { dict[Keys.imageUrl.rawValue] = $0.absoluteString }
        return dict
    }
}
