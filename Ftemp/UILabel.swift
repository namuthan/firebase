//
//  UILabel.swift
//  X4DWorkPackages
//
//  Created by Nabil Muthanna on 2016-12-08.
//  Copyright © 2016 Nabil Muthanna. All rights reserved.
//

import UIKit

enum UILabelType {
    case header, subheader, detail
}

extension UILabel {
    
    convenience init(type: UILabelType) {
        self.init()
        self.translatesAutoresizingMaskIntoConstraints = false
        self.numberOfLines = 0
        self.adjustsFontSizeToFitWidth = true
        self.minimumScaleFactor = 0.5
        self.lineBreakMode = .byWordWrapping
        
        switch type {
        case .header:
            self.font = MainStyleSheet.Font.header.font
            self.textColor = MainStyleSheet.Theme.main.color
        case .subheader:
            self.font =  MainStyleSheet.Font.subHeader.font
            self.textColor = .gray
        case .detail:
            self.textColor = MainStyleSheet.Theme.main.color
            self.font =  MainStyleSheet.Font.detail.font
        }
    }
    
    
}
