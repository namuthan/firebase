//
//  FIRDatabaseReference.swift
//  Ftemp
//
//  Created by Nabil Muthanna on 2017-03-18.
//  Copyright © 2017 Nabil Muthanna. All rights reserved.
//

import FirebaseDatabase


extension FIRDatabaseReference {
    
    func extendedPath(to ref: FIRDatabaseReference) -> String {
        return url.replacingOccurrences(of: ref.url, with: "/")
    }
}
