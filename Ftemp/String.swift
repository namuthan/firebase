//
//  String.swift
//  X4DWorkPackages
//
//  Created by Nabil Muthanna on 2016-12-13.
//  Copyright © 2016 Nabil Muthanna. All rights reserved.
//

import UIKit

extension UILabel {
    
    // get the estimated size for label with text and font
    func estimateSize(forText text: String, withFont font: UIFont, andSize size: CGSize) -> CGRect  {
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        return NSString(string: text).boundingRect(with: size, options: options, attributes: [NSFontAttributeName: font], context: nil)
    }
  
}

// sliding the navigation bar

extension UINavigationController {
    
    func hideBarWhenSwipe() {
        hidesBarsOnSwipe = true
    }
}

extension String {
    
    static var empty: String {
        get {
            return ""
        }
    }
}
